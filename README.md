# Team B2

### Team B2 ITT2 datacenter project
Be mindful of branches - the team has started to utilize the powers of Git.

#### Members consist of:
- Aleksis Kairiss,
- Alexander Bjørk Andersen,
- Daniel Rasmussen,
- Jacob Suurballe Petersen,
- Nikolaj Hult-Christensen,
- Sheriff Dibba

#### Pages
<a href="https://21s-itt2-datacenter-students-group.gitlab.io/team-b2-group/power-measuring/"> Website</a><br>

#### Guidelines  
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/branchingGuide.md">Branching Guide</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/gitGuidelines.md">Git Guidelines</a><br>

#### Documentation Links  
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/projectPlan.md">Milestones</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Pre-Mortem.md">Pre-Mortem</a><br>
<a href="https://padlet.com/dara28918/pphbz6u2qo3eflvt">System brainstorm</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Block%20diagram.pdf">system overview</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Use_cases.pdf">Use case Diagram</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Exercises/publish_potentiometer.py">MQTT introduction solution code (week 6, exercise 2)</a><br>
<a href="https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester">OME Education </a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Exercises/How_to_measure_current_cooling_ventilation.pdf">Measuring techniques research (week 7, exercise 1)</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/requirementDraft.md">System requirements draft</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Exercises/sensor_requirements.pdf">Sensor requirement draft</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Exercises/Sensors.md">List of possible sensors (week 7, exercise 3)</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Exercises/publish_tempC.py ">Sensor data via. RPi and MQTT solution code (week 7, exercise 4)</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/milestones">Milestones</a><br>
<a href="https://docs.google.com/document/d/1tfGMAO5aoPtzDC3XvHSrtsSoa8Yma-qmxCR7Ux6-49U/edit?usp=sharing">Scrum master, facilitator, secretary on rotation </a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Exercises/dataPersistenceUsingMongoDB.md">Data persistence using MongoDB exercise recreation steps and troubleshooting tips</a><br>
<a href="https://youtu.be/qDMYbmVYmlg">PoC Video</a><br>
<a href="https://youtu.be/nPGr5--uGKw">MVP Video</a><br>
<br>
<a href="https://docs.google.com/document/d/1BspfhEhvM6bj28MwgSGydFTKINpB1lp-0HDFgmcqnwI/edit">OME Meeting Minutes</a><br>
<a href="https://docs.google.com/document/d/1kgNIC0NchjgJtNOxhqQErdYmt1kLPgKll8sqRGpyBxw/edit#heading=h.oe1y0tp6cry3">Business Organisation</a><br>
<br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/sensors/-/blob/master/README.md">Sensors Readme</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/mqtt/-/blob/master/README.md">MQTT Readme</a><br>
<br>
<a href="https://docs.google.com/document/d/1p7Q4kVMKAJkqSQ1qQKQRLiMmJr5so1wH/edit#">Global Group Contract with team B2, A2, A4 and the OME's</a><br>
