Push your changes to the server at least once a day  
  
**Commit message guidelines**
1. Commit messages should be limited to 50 characters as a rule of thumb. The absolute maximum is 72
2. Capitalize the subject line
3. Do not end the subject line with a period
4. Use imperative mood in the subject line:  
	Be "commanding"  
	You should be able to write "If applied, this commit will" infront of your subject line, and it should make sense  
Good examples:  
	Fix failing CompositePropertySourceTests  
	Rework @PropertySource early parsing logic  
	Add tests for ImportSelector meta-data  
	Update docbook dependency and generate epub  
	Polish mockito usage  
	Modify configuration for feature  
Bad examples:  
	Re-adding ConfigurationPostProcessorTests after its brief removal in r814. @Ignore-ing the testCglibClassesAreLoadedJustInTimeForEnhancement() method as it turns out this was one of the culprits in the recent build breakage. The classloader hacking causes subtle downstream effects, breaking unrelated tests. The test method is still useful, but should only be run on a manual basis to ensure CGLIB is not prematurely classloaded, and should not be run as part of the automated build.  
	fixed two build-breaking issues: + reverted ClassMetadataReadingVisitor to revision 794 + eliminated ConfigurationPostProcessorTests until further investigation determines why it causes downstream tests to fail (such as the seemingly unrelated ClassPathXmlApplicationContextTests)  
	Tweaks to package-info.java files  
	Consolidated Util and MutableAnnotationUtils classes into existing AsmUtils  
	polishing  

**Commit size guidelines**  
Use the Atomic Approach  
	Commit each fix or task as a separate change  
	Only commit when a block of work is complete  
	Commit each layout change separately  
	Joint commit for layout file, code behind file, and additional resources  

**Branching guidelines**  
Never push commits directly to any mainline branch  
Always create a topic branch for your active development  
	A “topic” branch is simply a branch created for a specific development effort: adding a new feature, fixing a bug, etc.  
	If more than one person is working on the same feature, create a shared feature branch for everyone and a sub-branch that’s specifically for one person. Communicate often with your fellow developers to avoid complicated merge conflicts. When code is ready to be shared, push it in the shared feature branch; the other developer can then rebase the changes into their “private” branches. When the feature is complete, merge all changes into the shared feature branch and create a pull request from it  
	Never rebase shared commits  
	Never delete unmerged remote branches  

**Merging guidelines**  
	When you are ready to merge:  
		Rebase your branch so that it is synchronized with the appropriate main branch  
			Within your topic branch run git fetch to get the latest code from the server, then run git rebase origin/<YOUR_MAINLINE>  
	Create a pull request and have it reviewed  
	Only then should you merge your code  
	After merging your branch, the branch should be deleted, both locally and from the server  

**In case of fire**  
Git commit  
Git push  
Leave building  
