class error_checking:

    def diff_between_sensors(self, sensor_diff):
        # If the value/difference is over a certain value, return true + error message
        # to display on node-red
        if sensor_diff > 10:
            return True, "Difference between sensors above 10%!"
        else:
            return False

    def outside_range(self, sensor_values):
        # Check if sensor values are outside expected range
        if sensor_values > 3.400 or sensor_values < 0:
            return True, "Sensor outside range!"
        else:
            return False

