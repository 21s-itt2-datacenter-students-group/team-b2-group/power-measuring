import spidev
import time
import numpy

class sensor():
    def __init__(self, spi_interface):
        self.spi = spidev.SpiDev()  # Instantiate a spidev class, just like using an ADC
        self.spi.open(spi_interface, 0)  # Open SPI on SPI0 interfaces (spi hardware 0, bus 0). The SPI clock idles low
        self.spi.max_speed_hz = 50000  # Set SPI clockspeed. Max speed is based on the voltage provided to the MCP chip

    def read_adc(self, channel):
        # Construct message
        # VERY IMPORTANT: The MCP3202 takes 3 BYTES, the protocol looks like this:
        #
        #     Byte        0        1        2
        #     ==== ======== ======== ========
        #     Di   00000001 MCBxxxxx xxxxxxxx
        #     Do   xxxxxxxx xxx0RRRR RRRRRRRR

        # Di Byte 0 is the start byte. 7 zeroes followed by logic high tells the chip to starts transmitting
        # Di Byte 1 is the byte where we set commands:
        #   First bit (SGL/DIFF): M, Determines operating mode, to choose dual single ended mode, set to 1
        #   Second bit (ODD/SIGN): C, In dual single ended mode, is used to determine which channel to listen to, use 0 or 1
        #   Third bit (MSBF): B, Determines how the the bytes are sent back. Set to 1 for Most Signicant Bit First
        # The rest of the twelve bits doesn't matter, as the ADC doesn't care
        # The zero in Do, byte 1 is the null bit. We don't use that for anything directly
        # The R's are the results from the command we send to the ADC

        startByte = 0b00000001
        if channel == 0:
            commandByte = 0b10110000
        elif channel == 1:
            commandByte = 0b11110000
        else:
            # Raise an error if the channel is out of range
            raise NameError("Channel provided is out of range")
        trailingByte = 0b00000000  # ADC doesn't care what's in here

        # Save the result of the transmission to a variable.
        response = self.spi.xfer2([startByte, commandByte, trailingByte])
        # The variable is a list with a length of 3, with each element being a decimal based on a byte.
        # We want to convert that to one 12-bit long string we can then work with
        bits = ""
        for decimal_byte in response:
            # The format function takes a string input, which we then tell tell it to convert to a single byte
            # Which is then concatenated to the bits variable
            bits += format(decimal_byte, '008b')
        # We then finally type-cast this 12bit long string to decimals, which is then returned
        return int(bits, 2)
