from gpiozero import LED

class error_LED():

    def __init__(self, pin):
        self.led = LED(pin)

    def blink_DC(self):  # Blinks 1 time / sec by default
        self.led.blink() 

    def blink_difference(self): # Blinks 2 times / sec
        self.led.blink(on_time=0.5)

    def blink_threshold(self): # Constant on if there is an error
        self.led.on()

    def blink_OFF(self):
        self.led.off()
        
