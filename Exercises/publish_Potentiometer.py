import time
from ADCDevice import *
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt   #importing the library itself, as a client

adc = ADCDevice()    #Defines an ADCDevice Class Object
pin = 7    #Pin 7 = GPIO 4
broker_address="mqtt.eclipseprojects.io"   #public broker from the exercise
client = mqtt.Client("P1")   #creates new instance, P1 is just what the exercise says.
client.connect(broker_address)   #connect

def setup():
    global adc        
    global p    #Start the LED setup
    GPIO.setmode(GPIO.BOARD)    #Defines the board setup
    GPIO.setup(pin,GPIO.OUT)    #Make pin x output pwr
    p = GPIO.PWM(pin,1000) #1000hz on pin x Pulse Wave Modulation
    p.start(0)
        
def loop():
    while True:
        value = adc.analogRead(0)    #Reads ADC value of channel 0
        p.ChangeDutyCycle(value * 100/255)    #Defines DutyCycle map
        voltage = value / 255.0 * 3.3    #Calculates voltage in 8-bit (255 resolution).
        print ("Checking voltage: %.2f"%(voltage))    #Setting values via % in string
        client.publish("voltageLevelB2",voltage)   #publish, and publish random No from 0-10
        time.sleep(1)
        
def destroy():
    adc.close()
    GPIO.cleanup()
    
if __name__ == '__main__':    #Program entrance
    print ("Program is booting, starting")
    try:
        setup()
        loop()
    except KeyboardInterrupt:    # CTRL+C to end
        destroy()