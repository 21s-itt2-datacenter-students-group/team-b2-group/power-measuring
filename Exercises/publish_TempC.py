import RPi.GPIO as GPIO
import time
import math
from ADCDevice import *
import paho.mqtt.client as mqtt   #importing the library itself, as a client

adc = ADCDevice()
broker_address="mqtt.eclipseprojects.io"   #public broker from the exercise
client = mqtt.Client("P1")   #creates new instance, P1 is just what the exercise says.
client.connect(broker_address)   #connect

def setup():
    global adc #checking for either ADC module
    if(adc.detectI2C(0x48)):
        adc = PCF8591()
    elif(adc.detectI2C(0x4b)):
        adc = ADS7830()
    else:
        print("No I2C module found, shutting program down");
        exit(-1)
        destroy()
        
def loop():
    while True:
        value = adc.analogRead(0)    #ADC value from A0 pin
        voltage = value / 255.0 * 3.3    #calculate voltage
        Rt = 10 * voltage / (3.3 - voltage)    #calculate resistance value of thermistor
        tempK = 1/(1/(273.15 + 25) + math.log(Rt/10)/3950.0)    #calculate temperature (Kelvin)
        tempC = tempK -273.15    #calculate temperature (Celsius)
        print ("Temp in C: "+str(tempC))
        time.sleep(1)
        client.publish("temperatureLevelB2",tempC)   #publish, and publish random No from 0-10

def destroy():
    adc.close()
    GPIO.cleanup()
    
if __name__ == '__main__':  # Program entrance
    print ('Program is starting')
    setup()
    try:
        loop()
    except KeyboardInterrupt: # Press ctrl-c to end the program.
        destroy()
        
    
