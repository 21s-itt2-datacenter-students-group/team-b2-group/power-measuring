import random
import collections

realValue = random.uniform(0.0,100.0)	#uniform is used for float, and not int
maxNoise = 100.0
floatList = []
buffer = collections.deque(maxlen=5)
res = []
resErrs = []

def getNoisy(realValue, maxNoise):
	randomNoise = random.uniform(-maxNoise,maxNoise)
	randNumb = realValue + randomNoise
	return randNumb

def getMean(floatList):
	mean = sum(floatList)/len(floatList)	#sum = total of content of the list
	return mean

for i in range(5):
	value = getNoisy(4,1) # why 4 and 1?
	buffer.append(value)

value = getNoisy(4,1)
buffer.append(value)

print(buffer)

#Creates random values and puth them in a buffer (size 5), then finds the mean and adds that to a list called res. It does this 20 times.
for i in range(20):
	for j in range(5):
		value = getNoisy(4,1) #placeholder values
		buffer.append(value)
	res.append(getMean(buffer))

print("this is our res", res)

#adds the size of errors to a list
for n in res:
	resErrs.append(abs(res[int(n)] - 4))

print ("this is our res error", resErrs)

print("this is our mean error value:")
print(getMean(resErrs))
