import time
from ADCDevice import *
import paho.mqtt.client as mqtt

adc = ADS7830()    #defines adc type
address="hobelab-laptop.duckdns.org"   #or mqtt.eclipseprojects.io for test. hobelab-laptop.duckdns.org otherwise
client = mqtt.Client("P1")   #creates new instance P1
client.username_pw_set("TeamB2","mcp3202")   #sets username and password for entry
client.connect(address)
topic = "B2"   #primary topic for sending readings
topic2 = "B2-E"   #secondary topic for sending error messages
        
def loop():
    while True:
        value = adc.analogRead(0)
        voltage = value / 255.0 * 4    #calculates voltage in 8-bit (255 resolution) and converts to 3.3.
        voltage = round(voltage, 3)
        print ("Voltage value: %.2f"%(voltage))    #print for confirmation
        
        if (voltage < 0.5 or voltage > 3.3):   #if statement for errors
            print("Voltage readings are above 3.3 volts!")
            client.publish(topic2, "Voltage readings are above 3.3 volts!")
        else:        
            client.publish(topic, float(voltage))   #rounding up to 3 decimals
            time.sleep(0.1)
        
def destroy():
    adc.close()   #cleanup
    print("Stopping...")
    exit(0)
    
if __name__ == '__main__':
    print ("Program is booting, starting")
    try:
        loop()
    except KeyboardInterrupt:    # CTRL+C to end
        destroy()
