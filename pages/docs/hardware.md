# Hardware
## Electronics
The system consists of 2 current sensors (AS-103) connected to a dual 12bit ADC (mcp3202), and LED used for physically displaying errors. These components are connected to the GPIO pins of a Raspberry Pi using a serial peripheral connection.  
In the picture below, we see the wiring diagram of the system.  
<img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/raw/master/pages/img/wiring_diagram.png" alt="Wiring Diagram"><br>  
The MCP3202 is connected to the raspberry pi using serial communication, and takes an analog input from each of the current sensors. The LED is connected to a GPIO pin and ground.  
The values of R1 and R2 depend on what current is expected to flow through the current sensors. 50mA max was used for testing the system.  
The AS-103, a toroidal current transformer (Talema), has a single primary winding (the wire going through it) and 300 secondary winding, knowing this, the amp turn equation can be used to calculate the sensor output current:
<p align=center>IS = iP(NP/NS)<br>
IS = 50mA * (1/300)<br>
IS = 0.1667 mA</p><br>
The maximum output from the current sensors is 0.1667 mA.  
The required resistance to get 3.3 volts is then calculated using Ohm’s law:  
<p align=center>R = V / I<br>
R = 3.3 / 0,0001667<br>
R = 19.796</p><br>
The closest approximate resistor is a 20K resistor, so this was chosen to ensure that at maximum current flow, the output would be as close to 3.3 volts as possible.  
  
The chip select pin on the ADC is connected to the SPI0 CE0 pin on the Raspberry Pi, the CLK is connected to SPI0 SCLK, Digital out is connected to SPI0 MISO and Digital in is connected to SPI0 MOSI.  
The ADC is supplied with 5 volts rather than 3.3 volts as this allows it a higher conversion rate.
