#  Commit message guidelines

<p>1. Commit messages should be limited to 50 characters as a rule of thumb. The absolute maximum is 72</p>
<p>2. Capitalize the subject line</p>
<p>3. Do not end the subject line with a period</p>
<p>4. Use imperative mood in the subject line
<p>Be "commanding"</p>
<p>You should be able to write "If applied, this commit will" infront of your subject line, and it should make sense</p>

##  Good examples
<p>Fix failing CompositePropertySourceTests</p>
<p>Rework @PropertySource early parsing logic</p>
<p>Add tests for ImportSelector meta-data</p>
<p>Update docbook dependency and generate epub</p>
<p>Polish mockito usage</p>
<p>Modify configuration for feature</p>


##  Bad examples
<p>Re-adding ConfigurationPostProcessorTests after its brief removal in r814. @Ignore-ing the testCglibClassesAreLoadedJustInTimeForEnhancement() method as it turns out this was one of the culprits in the recent build breakage. The classloader hacking causes subtle downstream effects, breaking unrelated tests. The test method is still useful, but should only be run on a manual basis to ensure CGLIB is not prematurely classloaded, and should not be run as part of the automated build.
fixed two build-breaking issues: + reverted ClassMetadataReadingVisitor to revision 794 + eliminated ConfigurationPostProcessorTests until further investigation determines why it causes downstream tests to fail (such as the seemingly unrelated ClassPathXmlApplicationContextTests)
Tweaks to package-info.java files
Consolidated Util and MutableAnnotationUtils classes into existing AsmUtils
polishing</p>

##  Commit size guidelines
<p>Use the Atomic Approach</p>
<p>Commit each fix or task as a separate change.</p>
<p>Only commit when a block of work is complete.</p>
<p>Commit each layout change separately</p>
<p>Joint commit for layout file, code behind file, and additional resources</p>


##  Branching guidelines
<p>Never push commits directly to any mainline branch</p>
<p>Always create a topic branch for your active development</p>
<p>A “topic” branch is simply a branch created for a specific development effort: adding a new feature, fixing a bug, etc.</p>
<p>If more than one person is working on the same feature, create a shared feature branch for everyone and a sub-branch that’s specifically for one person.</p> 
<p>Communicate often with your fellow developers to avoid complicated merge conflicts. When code is ready to be shared, push it in the shared feature branch; the other developer can then rebase the changes into their “private” branches. When the feature is complete, merge all changes into the shared feature branch and create a pull request from it</p>
<p>Never rebase shared commits</p>
<p>Never delete unmerged remote branches</p>
<p>Merging guidelines</p>
<p>When you are ready to merge:</p>
<p>Rebase your branch so that it is synchronized with the appropriate main branch</p>
<p>Within your topic branch run git fetch to get the latest code from the server, then run git rebase origin/(YOUR_MAINLINE)</p>
<p>Create a pull request and have it reviewed only then should you merge your code</p>
<p>After merging your branch, the branch should be deleted, both locally and from the server</p>
<p>"In case of fire"</p>
<p>Git commit</p>
<p>Git push</p>
<p>Leave the building</p>
