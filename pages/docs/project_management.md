# Project Management

## Group contract
<p>IT- technology/Operational Maintenance Engineers</p>

### Group members:
<p>OME</p> 
<p>Brian Brosbøl (EES2019117@EDU.FMS.dk)</p>
<p>Mathias Skovlyst Mortensen (E20182042@edu.fms.dk)</p>
<p>Mads Barslund Andersen (EES2019108@edu.fms.dk)</p>

### ITT-Team A2
<p>Rune Mathias Petersen (Rmpe29519@edu.ucl.dk)</p>
<p>Greta Sumskaite (grsu28901@edu.uc.dk)</p>
<p>Benjamin Bom Christensen (bbch28792@edu.ucl.dk</p> 
<p>Julius Ingeli (juin28834@edu.ucl.dk)</p>
<p>Manisha Gurung(magu28891@edu.ucl.dk)</p>
<p>Oscar Harttung (oafh28885@edu.ucl.dk)</p>
<p>Balázs Erdős (bala0196@edu.ucl.dk)</p>

### ITT-Team A4
<p>Kajetan Przybysz (kapr28890@edu.ucl.dk)</p>
<p>Mansuri (mhmm28897@edu.ucl.dk)</p>

### ITT-Team B2
<p>Nikolaj Hult-Christensen (nihu28925@edu.ucl.dk)</p>
<p>Alexander Bjørk Andersen (aban28821@edu.ucl.dk)</p>
<p>Daniel Rasmussen (dara28918@edu.ucl.dk)</p>
<p>Aleksis Kairiss (alka2879@eal.ucl.dk)</p>
<p>Jacob Suurballe Petersen (jspe28926@edu.ucl.dk)</p>
<p>Sheriff Dibba (shdi28860@edu.ucl.dk)</p>


###  1. Goals:

<p>1.1.	The end product must not be affected by any conflict that might appear in the group.</p>
<p>1.2.	Everyone (The OME’s and the individual groups) must work together to make an end product that we can be proud of.</p>



###  2. Organization
<p>2.1 Preparation</p>
<p>2.1.1.	All members should be well prepared. If a group has questions or certain things they are struggling with, do not be afraid to reach out to other groups or OME’s (especially if it’s slowing down work).
Each meeting will have an agenda so everybody knows what will be discussed during that meeting.</p>
<p>2.2 Commitment</p>
<p>2.2.1.	It is implied that every group member is committed in every given task/project they are working on.</p> 




###  3. Communication
<p>3.1. Conflict handling</p>
<p>3.1.1.	Everyone in the group should prepare to give and receive criticism/feedback in a positive/constructive manner.</p>

<p>3.1.2.	If a group member is unhappy with something within the group or between groups/OEM’s, please speak up so it can be solved as soon as possible.</p>


###  3.2 Language
<p>3.2.1 	The default language is English for both verbal and written communication. If something is said or written in Danish, it must be translated to English.</p>
<p>3.3 Contact option</p>
<p>3.3.1 	A discord group has been made to cover the contact between all members of both the IT-technology education and the Operations and Maintenance Engineering education.</p>

<p>3.3.2	We use Google docs for file sharing and other related shared study materials</p>

<p>3.3.3	All group meetings as a rule of thumb should be held either through discord or zoom. Platform agreed upon before meeting.</p>


<p>3.3.4	Respectful and professional tone between everyone. </p>

<p>3.3.5	Please use a camera whenever possible</p>



###  4. Absence

<p>4.1	Every group must designate a person as the primary point of contact. This is also the person who will keep track of who will be present from their group during the meetings.</p>


<p>4.2 	If a team is not able to partake in a meeting, then it is necessary to inform the other party about their absence from the meeting.</p>

<p>4.3	If a meeting has to be cancelled, a new one should be scheduled in its place.</p>

<p>4.4	A group who refuses or no longer communicates with other groups or OME will not be tolerated, and will be considered a violation of the contract.</p>



###  5. Planning
<p>5.1.	For planning we will use a google datasheet with dates and times. That way everybody knows when meetings are held for each group.</p>

<p>5.1.1	The next meeting will be organized at the end of the current meeting.</p>

###  Group member signatures

<p>_____________________________Team B2________________________________</p>
<p>_____________________________Team A2________________________________</p>
<p>____________________________Team OME________________________________</p>
<p>____________________________________________________________________</p>
<p>____________________________________________________________________</p>
<p>____________________________________________________________________</p>

