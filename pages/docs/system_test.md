#System test plan

##Possible types of failures
Power loss
Loss of internet
Faulty sensors
Software failure (program crashes)
Hardware failure
Damaged sensor

##Ways to test each failure
###Power loss
RPI
Manual: Ping the RPI.
Server
Manual: Ping to the server.


###Loss of internet
RPI
Manual: ssh into RPI
Ping 8.8.8.8 (Google) from the RPI.
Server
Manual: ssh into the Server
Ping 8.8.8.8 (Google) from the server.


###Faulty sensors
ADC
Manual: Look at the sensors, and see if there is any physical damage.
Manual/Automatic: Apply a known voltage to the ADC, and see if the output matches.
Voltage sensor
Manual: Apply a known voltage to the Voltage sensor, and see if the output matches.
Automatic: Compare readings of the two voltage sensors using unit +testing.


###Software failure (program crashes)
####Python
- Manual: Check and see if the dashboard is updating.
- Automatic: Script running in the background periodically checking if the service is running, if it is not, send an error message using MQTT to the server.
- Node-red

- Manual: Manually input a value and see if it goes through the nodes properly.
- Inspect the debug sidebar .
- Automatic: Catch node outputs message to the d ashboard with the error.

####MQTT
- Manual: Check python output, and see if the broker is not reachable.
- Manual: See if the broker service is running on the server.
- Automatic: Script that checks if the broker service is running on the server, if it is not, output error message to the dashboard.

##What needs to be done
- Write a small script that runs a test, comparing a known voltage to the output of the ADC
- Write a script that compares the readings from the voltage sensors
- Write a script that checks if the python program is running, if it is not, send a message using MQTT. Add handling for this error in node-red.
- Set up manual node-red error input/checking.
- Set up a catch node in node-red that outputs a message to the dashboard.
- Add error message output on loss of connection to MQTT broker to the python script.
- Write a script that checks if MQTT is running, if it is not, output an error message to node-red.



