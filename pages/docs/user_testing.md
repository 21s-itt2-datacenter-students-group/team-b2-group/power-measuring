# User Testing
User testing refers to a technique used in the design process to evaluate a product, feature or prototype with real users.

It is used as part of our user-centered design process which helps us to gain empathy of the people in this case the OME's by testing the prototype with them directly and understanding how they feel about it.

## User Testing methods used
In our scenario we used usuability testing method to test our MVP with the OME (Product Owners).
Usuability testing evaluates the degree to which the system can be used by specified  users with effectiveness, efficiency and satisfaction in a specified context during this testing, the product owners shows shows great satisfaction and show few observations which we intent to add or amend in our final product.

## Limitations
Since it was done purely online, we were unable to make eye-contact or make direct observation with regards to their behaviour to the MVP product.
