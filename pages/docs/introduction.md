# Introduction - What are we doing

For this semester, group B2 is collaborating with Operation Maintenance Engineers from Fredericia Maskinmesterskole, using the agile framework scrum.
The goal is to create a power monitoring system which will alert the engineers in case of a problem (loss of power, power spikes, voltage outside expectations etc.).

To accomplish this, we're using a power sensor, a raspberry pi which will communicate through MQTT to a dashboard based on nodered. It is this dashboard that the engineers will be using.