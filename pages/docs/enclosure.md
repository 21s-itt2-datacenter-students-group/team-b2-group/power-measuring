An enclosure for the product was designed in 3d CAD software. Using precise measurements of the raspberry pi, and a veroboard, it was possible to design an enclosure that snugly fit both of these, and make sure that components would be able to be placed in easily accessible places.  
  
The design features holes for 2 current sensors, an LED, power cable, screws to hold Raspberry pi and vero board in place, holes for “airflow” and a hole for the ADC that makes it possible to easily replace it in case it breaks.  
The dimensions of the enclosure are Length: 103mm, Width: 71mm, Height: 37,8mm.  
The 3d Model is seen in the picture below:  
<img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/raw/master/pages/img/3d_enclosure.png" alt="3D enclosure"><br>
The design was made in Autodesk Fusion 360, made to fit a 6mm thick material. When the design was done, each component of the design was exported as DXF files, and assembled in Adobe illustrator 2020, from where it was exported to the laser cutter that then cut the enclosure out of 6mm MDF board.  
<img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/raw/master/pages/img/enclosure_closed.png" alt="Closed enclosure"><br>
<img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/raw/master/pages/img/enclosure_open.png" alt="Open enclosure">