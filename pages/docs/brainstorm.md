                                                          **Team B2 brainstorm**
                                                _Datacenter brainstorm 10/2-2021 What system will we be building_
 

**UPS load factor**
How much is each ups being utilized, aiming for the most efficient load factor for each ups

**Sensor system**
Heartbeat system, meaning that if a sensor unit (raspberry pi) stops sending messages, it is assumed that the unit went down through loss of power (needs redundancy!)

**Monitor load balance percent**
Are power cables to each rack being utilized the same amount as each other. This will make sure that wires are not overheated

**Circuit breaker monitoring**
When circuit breakers are not monitored properly, they can become overloaded and cause downtime, create bottle necks in the power chain, or be underutilized causing you to not get the most out of your power resources  
For the most complete data, monitor the budget load, rating, load per phase leg, highest leg, lowest two legs, phase unbalance, and total load of each breaker at each level of the power chain hierarchy including rack PDUs, branch circuits, power panels, oor PDUs, and the UPS bank. 

**Tracking power failover redundancy**
How much power can be supplied to each rack in case of a blackout or ups failure. 
This could be applied to only critical racks.

**Power quality monitoring**
Measure voltage dips/brown outs. Quickly discover if there is a frequency problem as well.
Days/Hours of power capacity remaining
Compare power consumption with backup power capacity
Measure peak power load per cabinet
Alerts when power exceeds threshhold (something is wrong)

**Power loss**
System to measure loss of power in different areas of the data center, with redudancy (through relays?).

**Stranded power capacity per rack**
Data center managers will often allocate more power to each rack than is actually demanded by the IT equipment. This causes stranded power that can be deployed elsewhere in the data center to save costs. For a single rack, a few kilowatts of stranded power may seem unremarkable, but when you factor in hundreds or thousands of racks, stranded power could account for as much as 50 percent of all available power. Monitor power consumption in your data center to identify stranded capacity. Then, deploy that power with con dence and delay spending millions to build your next data center.


**Remember these important things**


- Placement
- Coding
- Dimensions: How much space do we have to work with?
- COVID-19:
- Physically building and testing the product
- POC
- Simplication - KISS (for OME's e.g.)
- Communication between units (sensors, displays, and so forth)
- OME's in uence
- Electric circuit(Orcad, Matlap)
- Budget
- Enclosure
- Measurements
- Backup generators systems
- Safety, both for tech and people Automatic transfer switch (ATS)
- Power distribution unit (PDU)
- Redundancy

